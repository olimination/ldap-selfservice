$(document).ready(function() {

	$(".js-remove-user").click(function() {
		var removeUrl = $(this).attr("data-remove-url");
		var csrfToken = $("meta[name='_csrf']").attr("content");
		var csrfHeader = $("meta[name='_csrf_header']").attr("content");

		$.ajax({
			url : removeUrl,
			type : "DELETE",
			beforeSend : function(xhr) {
				xhr.setRequestHeader(csrfHeader, csrfToken);
			},
			dataType : "json"
		})
		// Code to run if the request succeeds (is done);
		// The response is passed to the function
		.done(function(json) {
			$("div.flash-message-success").html(json.message);
			window.location = json.redirect;
		})
		// Code to run if the request fails; the raw request and
		// status codes are passed to the function
		.fail(function(xhr, status, errorThrown) {
			$("div.flash-message-error").html(status + " / " + errorThrown);
		});
	});

});