package org.olimination.ldapselfservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LdapSelfServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LdapSelfServiceApplication.class, args);
	}

}
