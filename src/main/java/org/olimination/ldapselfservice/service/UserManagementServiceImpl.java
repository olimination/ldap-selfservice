package org.olimination.ldapselfservice.service;

import java.util.List;

import javax.naming.Name;

import org.olimination.ldapselfservice.LdapConfiguration;
import org.olimination.ldapselfservice.dao.PersonRepo;
import org.olimination.ldapselfservice.model.Group;
import org.olimination.ldapselfservice.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.security.ldap.LdapUtils;
import org.springframework.stereotype.Service;

/**
 * @author olimination
 */
@Service
public class UserManagementServiceImpl implements UserManagementService {

	@Autowired
	private LdapConfiguration ldapConfig;

	@Autowired
	private LdapTemplate ldapTemplate;

	@Autowired
	private PersonRepo personRepo;

	@Override
	public Person getPersonByUid(String uid) {
		return personRepo.findByUid(uid);
	}

	@Override
	public void changeUserPassword(Person person, String pw) {
		person.setUserPassword(pw);
		personRepo.update(person);
	}

	@Override
	public void addUser(Person person) {
		personRepo.create(person);
	}

	@Override
	public List<Person> getAllPersons() {
		return personRepo.findAll();
	}

	@Override
	public void removeUser(String uid) {
		Person person = personRepo.findByUid(uid);
		personRepo.delete(person);
	}

	@Override
	public boolean checkUserPassword(String uid, String password) {
		AndFilter filter = new AndFilter();
		filter.and(new EqualsFilter("objectclass", "person")).and(new EqualsFilter("uid", uid));
		return ldapTemplate.authenticate(ldapConfig.getUserSearchBase(), filter.toString(), password);
	}

	@Override
	public void updateUser(Person person) {
		personRepo.update(person);
	}

	@Override
	public List<Group> getGroupsOfPerson(Person person) {
		return personRepo.getGroupsOfPerson(generateFullPersonDn(person).toString());
	}

	@Override
	public void addGroupToUser(Person person, Group group) {
		group.addMember(generateFullPersonDn(person));
		ldapTemplate.update(group);
	}

	@Override
	public Name generateFullPersonDn(Person person) {
		String rootDn = LdapUtils.parseRootDnFromUrl(ldapConfig.getUrl());
		return LdapNameBuilder.newInstance(rootDn).add(person.getDn()).build();
	}

	@Override
	public void removeGroupFromUser(Person person, Group group) {
		group.removeMember(generateFullPersonDn(person));
		ldapTemplate.update(group);
	}
}
