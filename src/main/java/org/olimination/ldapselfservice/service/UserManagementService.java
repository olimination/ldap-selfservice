package org.olimination.ldapselfservice.service;

import java.util.List;

import javax.naming.Name;

import org.olimination.ldapselfservice.model.Group;
import org.olimination.ldapselfservice.model.Person;

/**
 * @author olimination
 */
public interface UserManagementService {

	Name generateFullPersonDn(Person person);
	
	Person getPersonByUid(String uid);

	/**
	 * Checks the provided password of the given userId and returns true if an
	 * LDAP authentication was successful.
	 * 
	 * @param uid
	 *            The current user id.
	 * @param password
	 *            The current user password, not encoded.
	 * @return Returns true if authentication was successful, else false.
	 */
	boolean checkUserPassword(String uid, String password);

	void changeUserPassword(Person person, String password);

	void addUser(Person person);
	
	void updateUser(Person person);

	void removeUser(String uid);

	List<Person> getAllPersons();
	
	List<Group> getGroupsOfPerson(Person person);
	
	void addGroupToUser(Person person, Group group);
	
	void removeGroupFromUser(Person person, Group group);
}
