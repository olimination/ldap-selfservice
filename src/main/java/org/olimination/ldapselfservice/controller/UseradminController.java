package org.olimination.ldapselfservice.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.olimination.ldapselfservice.dao.GroupRepo;
import org.olimination.ldapselfservice.model.Group;
import org.olimination.ldapselfservice.model.Person;
import org.olimination.ldapselfservice.model.PersonAddEditForm;
import org.olimination.ldapselfservice.model.PersonChangePasswordForm;
import org.olimination.ldapselfservice.service.UserManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author olimination
 */
@Controller
public class UseradminController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserManagementService userManagementService;

	@Autowired
	private GroupRepo groupRepo;

	@RequestMapping(value = "/private/users", method = GET)
	public String listUsers(Model model) {
		model.addAttribute("users", userManagementService.getAllPersons());
		return "private/useradmin/list";
	}

	@RequestMapping(value = "/private/users/{uid}", method = DELETE)
	@ResponseBody
	public ResponseEntity<String> removeUser(HttpServletRequest request, @PathVariable String uid) {

		try {
			userManagementService.removeUser(uid);
		} catch (Exception e) {
			log.error("Could not remove user. ", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body("{\"message\":\"Removing the user has failed.\"}");
		}

		String redirectPath = "/".equals(request.getContextPath()) ? request.getContextPath() + "private/users"
				: request.getContextPath() + "/private/users";
		return ResponseEntity
				.ok("{\"message\":\"User has been removed successfully\",\"redirect\":\"" + redirectPath + "\"}");
	}

	@RequestMapping(value = "/private/users/{userId}", method = GET)
	public String showEditUserForm(@PathVariable String userId, PersonAddEditForm formModel, Model model) {

		Person person = userManagementService.getPersonByUid(userId);

		formModel.setGivenName(person.getGivenName());
		formModel.setMail(person.getMail());
		formModel.setSurname(person.getSurname());
		formModel.setUid(userId);

		model.addAttribute("allGroups", groupRepo.findAll());
		model.addAttribute("groupsOfPerson", userManagementService.getGroupsOfPerson(person));

		return "private/useradmin/edit";
	}

	@RequestMapping(value = "/private/users/{userId}", method = POST)
	public String editUser(@PathVariable String userId, @Valid PersonAddEditForm formModel, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		Person person = userManagementService.getPersonByUid(userId);

		// Ignore pw field for edit forms, should be optional
		if (bindingResult.hasErrors() && hasOnlyPasswordSizeError(bindingResult)) {
			return "private/useradmin/edit";
		}

		String newUid = formModel.getUid();

		person.setGivenName(formModel.getGivenName());
		person.setSurname(formModel.getSurname());
		person.setFullName();
		person.setMail(formModel.getMail());
		person.setUid(newUid);

		if (!bindingResult.hasFieldErrors(Person.USER_PASSWORD_FIELD)) {
			log.debug("Setting new user password now.");
			person.setUserPassword(formModel.getUserPassword());
		}

		userManagementService.updateUser(person);

		if (formModel.getGroups() != null) {
			handleGroups(person, formModel.getGroups());
		}

		redirectAttributes.addFlashAttribute("success", true);

		return "redirect:/private/users/" + newUid;
	}

	private void handleGroups(Person person, List<String> groupsFromRequest) {
		for (Group group : groupRepo.findAll()) {
			if (groupsFromRequest.contains(group.getName())) {
				log.debug("Adding group: " + group.getName() + " to user: " + person.getUid());
				userManagementService.addGroupToUser(person, group);
			} else {
				log.debug("Removing group: " + group.getName() + " from user: " + person.getUid());
				userManagementService.removeGroupFromUser(person, group);
			}
		}
	}

	private boolean hasOnlyPasswordSizeError(BindingResult bindingResult) {
		List<FieldError> errorList = bindingResult.getFieldErrors(Person.USER_PASSWORD_FIELD);
		return errorList.stream().anyMatch(n -> n.getCode().equals("Size"))
				&& errorList.stream().noneMatch(n -> n.getCode().equals("NotEmpty"));
	}

	@RequestMapping(value = "/private/users/add", method = GET)
	public String showAddUserForm(PersonAddEditForm formModel) {
		return "private/useradmin/add";
	}

	@RequestMapping(value = "/private/users", method = POST)
	public String addUser(@Valid PersonAddEditForm formModel, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {

		if (bindingResult.hasErrors()) {
			return "private/useradmin/add";
		}

		Person person = new Person();
		person.setGivenName(formModel.getGivenName());
		person.setSurname(formModel.getSurname());
		person.setFullName();
		person.setUid(formModel.getUid());
		person.setMail(formModel.getMail());
		person.setUserPassword(formModel.getUserPassword());

		userManagementService.addUser(person);

		redirectAttributes.addFlashAttribute("success", true);

		return "redirect:/private/users/add";
	}

	@RequestMapping(value = "/private/users/password", method = GET)
	public String showChangePasswordForm(PersonChangePasswordForm personChangePasswordForm) {
		return "private/useradmin/changepassword";
	}

	@RequestMapping(value = "/private/users/password", method = POST)
	public String changePassword(Principal principal, @Valid PersonChangePasswordForm personChangePasswordForm,
			BindingResult bindingResult, final RedirectAttributes redirectAttributes) {

		if (!userManagementService.checkUserPassword(principal.getName(), personChangePasswordForm.getOldpwd())) {
			bindingResult.rejectValue("oldpwd", "forms.passwordchange.oldpwnotmatching");
		}

		if (bindingResult.hasErrors()) {
			return "private/useradmin/changepassword";
		}

		Person person = userManagementService.getPersonByUid(principal.getName());
		userManagementService.changeUserPassword(person, personChangePasswordForm.getNewpwd());

		redirectAttributes.addFlashAttribute("success", true);

		return "redirect:/private/users/password";
	}
}
