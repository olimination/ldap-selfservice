package org.olimination.ldapselfservice.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author olimination
 */
@Controller
public class MainController {

	@RequestMapping(value = "/private/home", method = GET)
	public String home() {
		return "private/home";
	}
}
