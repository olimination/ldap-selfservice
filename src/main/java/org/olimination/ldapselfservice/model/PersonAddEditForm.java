package org.olimination.ldapselfservice.model;

import java.nio.charset.Charset;
import java.util.List;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class PersonAddEditForm {

	@NotEmpty
	private String uid;

	@NotEmpty
	private String givenName;

	@NotEmpty
	private String surname;

	@NotEmpty
	private String mail;

	@NotEmpty
	@Size(min = 8, max = 50)
	private byte[] userPassword;

	private List<String> groups;

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public byte[] getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword.getBytes(Charset.forName("UTF-8"));
	}

	public void setUserPassword(byte[] userPassword) {
		this.userPassword = userPassword;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public List<String> getGroups() {
		return groups;
	}

	public void setGroups(List<String> groups) {
		this.groups = groups;
	}

}
