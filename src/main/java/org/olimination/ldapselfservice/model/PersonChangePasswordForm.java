package org.olimination.ldapselfservice.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.ScriptAssert;

/**
 * @author olimination
 */
@ScriptAssert(lang = "javascript", script = "_this.newpwd == _this.confirmNewpwd", message = "{forms.passwordchange.notmatching}")
public class PersonChangePasswordForm {

	@NotEmpty
	private String oldpwd;

	@Size(min = 8, max = 50)
	@NotEmpty
	private String newpwd;

	private String confirmNewpwd;

	public String getOldpwd() {
		return oldpwd;
	}

	public void setOldpwd(String oldpwd) {
		this.oldpwd = oldpwd;
	}

	public String getNewpwd() {
		return newpwd;
	}

	public void setNewpwd(String newpwd) {
		this.newpwd = newpwd;
	}

	public String getConfirmNewpwd() {
		return confirmNewpwd;
	}

	public void setConfirmNewpwd(String confirmnewpwd) {
		this.confirmNewpwd = confirmnewpwd;
	}
}
