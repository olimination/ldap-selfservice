package org.olimination.ldapselfservice.model;

import java.nio.charset.Charset;

import javax.naming.Name;

import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Attribute.Type;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

/**
 * @author olimination
 */
@Entry(objectClasses = { "inetOrgPerson", "person", "top" }, base = "ou=users")
public final class Person {
	
	public static final String USER_PASSWORD_FIELD = "userPassword";

	@Id
	private Name dn;

	@Attribute(name = "cn")
	@DnAttribute(value = "cn", index = 1)
	private String fullName;

	@Attribute(name = "sn")
	private String surname;

	private String givenName;

	private String uid;

	private String mail;

	@Attribute(type = Type.BINARY)
	private byte[] userPassword;

	public String getFullName() {
		return fullName;
	}

	/**
	 * Make sure to set first givenName and surname!
	 */
	public void setFullName() {
		this.fullName = givenName + " " + surname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public byte[] getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword.getBytes(Charset.forName("UTF-8"));
	}

	public void setUserPassword(byte[] userPassword) {
		this.userPassword = userPassword;
	}

	public Name getDn() {
		return dn;
	}
	
	@Override
	public String toString() {
		return givenName + " " + surname + "(" + uid + ")";
	}

}