package org.olimination.ldapselfservice.dao;

import java.util.List;

import org.olimination.ldapselfservice.model.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author olimination
 */
@Repository
public class GroupRepo {

	@Autowired
	private LdapTemplate ldapTemplate;

	public List<Group> findAll() {
		return ldapTemplate.findAll(Group.class);
	}

	public void update(Group group) {
		ldapTemplate.update(group);
	}
}
