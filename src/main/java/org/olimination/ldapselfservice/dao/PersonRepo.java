package org.olimination.ldapselfservice.dao;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import java.util.List;

import org.olimination.ldapselfservice.model.Group;
import org.olimination.ldapselfservice.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.stereotype.Repository;

/**
 * @author olimination
 */
@Repository
public class PersonRepo {

	@Autowired
	private LdapTemplate ldapTemplate;

	public Person create(Person person) {
		ldapTemplate.create(person);
		return person;
	}

	public Person findByUid(String uid) {
		return ldapTemplate.findOne(query().where("uid").is(uid), Person.class);
	}

	public void update(Person person) {
		ldapTemplate.update(person);
	}

	public void delete(Person person) {
		ldapTemplate.delete(person);
	}

	public List<Person> findAll() {
		return ldapTemplate.findAll(Person.class);
	}

	public List<Person> findByLastName(String lastName) {
		return ldapTemplate.find(query().where("sn").is(lastName), Person.class);
	}

	public List<Group> getGroupsOfPerson(String fullPersonDn) {
		AndFilter filter = new AndFilter();
		filter.and(new EqualsFilter("objectclass", "groupOfUniqueNames"))
				.and(new EqualsFilter("uniqueMember", fullPersonDn));

		return ldapTemplate.find(query().filter(filter), Group.class);
	}
}
