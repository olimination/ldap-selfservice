FROM alpine:3.3
RUN apk --update add openjdk8-jre
VOLUME /tmp
ADD ldap-selfservice-app-0.1.0-SNAPSHOT.jar app.jar
ADD application-prod.properties application-prod.properties
RUN sh -c 'touch /app.jar'