# LDAP Self-Service web application #

Simple LDAP Self-Service web app based on Spring Boot.

It features:

* Change password
* Add new user
* List users

## Development - Getting Started ##

    mvn install
    mvn spring-boot:run -Dspring.profiles.active=local
    
Runs the Spring Boot application with the _local_ profile which enables you to configure your local LDAP server for testing purposes.

## Running as single jar ##

Use for example this command:

    java -jar ldap-selfservice-app-VERSION.jar --server.port=8081 --spring.profiles.active=prod
    
This will run the web application on port 8081 with the _prod_ configuration profile.

## Run with Docker ##

Make sure that you have all needed files in one folder selected by you:
* ldap-selfservice-app-VERSION.jar
* application-prod.properties

Create a Docker image with the provided Dockerfile

    sudo docker build -t myapps/ldap-selfservice-app .

Then run a container with this image

    sudo docker run -i -t -e "SPRING_PROFILES_ACTIVE=prod" -e "SERVER_PORT=8081" --link ldap1:ldap1 -v $(pwd):/appconfig -p 8081:8081 --name ldapss1 olbur/ldap-selfservice-app

The /appconfig mounted volume can be used if you still need to to any configuration for application-prod.properties.

Start then the application with:

    java -jar app.jar
    
The profile and server port are then taken from then environment variables.